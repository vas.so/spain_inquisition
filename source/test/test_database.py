import random
from datetime import datetime
from random import choice

import pytest
from peewee import IntegrityError

from source.model.entities import get_database, with_connection
from source.model.entities import Ticket, TicketStatus
from source.model.entities import TicketStatusLog
from source.test.mesaures import time_it

tickets_range = 5000
user_range = tickets_range // 100
logs_range = tickets_range * 5


@time_it
def mock_users():
    users_ = [TelegramUser(id=i, first_name=f'Some name {str(i)}', last_name=f'Some surname {str(i)}')
              for i in range(user_range)]
    with pytest.raises(IntegrityError):
        TelegramUser.bulk_create(users_)
        return users_


@time_it
def mock_tickets(users_):
    tickets_ = [Ticket(id=i,
                       title=f'title {i}',
                       description=f'description {i}',
                       created_by=choice(users_).id,
                       created_at=datetime.now(),
                       status=choice(TicketStatus.mapping),
                       is_open=False,
                       last_activity=datetime.now())
                for i in range(tickets_range)]
    with pytest.raises(IntegrityError):
        Ticket.bulk_create(tickets_)
        return tickets_


@time_it
def mock_logs(tickets_):
    logs_ = [TicketStatusLog(ticket_id=choice(tickets_).id,
                             status=choice(TicketStatus.mapping),
                             date=datetime.now())
             for _ in range(logs_range)]
    with pytest.raises(IntegrityError):
        TicketStatusLog.bulk_create(logs_)
        return logs_


def test_with_connection():
    database = get_database()
    assert database.is_closed()
    lam = lambda x, y: (x, y)
    result = with_connection(require_closing=True)(lam)(0, 1)
    assert database.is_closed()
    assert result == (0, 1)
    result = with_connection(require_closing=False)(lam)(0, 1)
    assert database.is_closed() is False
    assert result == (0, 1)
    database.close()


def test_exists():
    uid = random.randint(10000, 40000)
    f_name = 'First Name'
    l_name = 'Last Name'
    database = get_database()
    with database.atomic():
        TelegramUser.create(id=uid, first_name=f_name, last_name=l_name)
    assert TelegramUser.exists(TelegramUser.id, uid) is True
    assert TelegramUser.exists(TelegramUser.first_name, f_name) is True
    assert TelegramUser.exists(TelegramUser.last_name, l_name) is True
    with database.atomic():
        result = TelegramUser.delete_by_id(uid)
    assert TelegramUser.exists(TelegramUser.id, uid) is False
    assert TelegramUser.exists(TelegramUser.first_name, f_name) is False
    assert TelegramUser.exists(TelegramUser.last_name, l_name) is False
    database.close()
    assert database.is_closed()


def test_select_equals():
    pass


def test_get_equals():
    pass


def test_select_with_operator():
    pass


def test_get_with_operator():
    pass


def test_get_attrib():
    pass
