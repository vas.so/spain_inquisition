import json
from random import choice
from typing import List, Tuple

from babel.dates import format_datetime
from requests import get, Response
from telegram import Update, ReplyKeyboardMarkup, User, Chat, Message, InlineKeyboardMarkup, \
    InlineKeyboardButton

from source.config import Config


def extract_photo(response: Response, api_dict: dict, api_key: str) -> str:
    """ Extrating photo URL from reponse object

    :param response: response object
    :param api_dict: dict of possible APIs
    :param api_key:
    :return: string URL of photo
    """
    if response.status_code == 200:
        content = json.loads(response.content.decode(Config().default_encoding))
        if isinstance(content, list):
            content = content[0]
        key = api_dict.get(api_key)
        return content.get(key)


def random_animal_photo() -> str:
    """ Retreives photo of animal from random api

    :return: string URL of photo
    """
    api_dict = Config().animal_api_fetch_map
    api_key = choice([key for key in api_dict.keys()])
    response = get(api_key)
    photo = extract_photo(response, api_dict, api_key)
    if not photo:
        for api_url in api_dict.keys():
            try:
                photo = extract_photo(get(api_url), api_dict, api_key)
            except Exception:
                pass
            else:
                if photo:
                    break
    return photo


def random_phrase() -> str:
    """ Retreives random phrase from website

    :return: string with retreived text or empty string
    """
    from bs4 import BeautifulSoup
    try:
        soup = BeautifulSoup(get('https://theytoldme.com').content, 'html.parser')
        text = f'{soup.strong.text.strip()} {soup.p.text.strip()}'
    except:
        return ''
    else:
        return text


def random_cocktail() -> Tuple[str, str]:
    """ Retreives and formats cocktail recipe

    :return: tuple of cocktail photo url and instructions
    """
    response = get(Config().cocktails_api)
    drink_photo_url = text = ''
    if response.status_code == 200:
        content = json.loads(response.content.decode(Config().default_encoding))
        cocktail_json = content['drinks'][0]
        drink_photo_url = cocktail_json['strDrinkThumb']
        drink_name = cocktail_json['strDrink']
        ingredients = [value for key, value in cocktail_json.items() if key.startswith('strIngredient') and value]
        measures = [value for key, value in cocktail_json.items() if key.startswith('strMeasure') and value]
        ingredients = '\n'.join([f'{ingredient} - {measure}' for ingredient, measure in zip(ingredients, measures)])
        instruction = cocktail_json['strInstructions']
        text = '<b>{}:</b>\n{}\n<b>Ingredients:</b>\n{}'.format(drink_name, instruction, ingredients)
    return drink_photo_url, text


def user_chat_message(update: Update) -> Tuple[User, Chat, Message]:
    """ Shortcut for extracting user, chat and message from Update object

    :param update: Arbitrary Update object
    :return: Tuple of User, Chat and Message objects
    """
    return update.effective_user, update.effective_chat, update.effective_message


def user_mention(user_id: int, full_name: str) -> str:
    """ Wrapping User name and last name into HTML user mention

    :param user_id: Arbitrary User ID
    :param full_name: User first and last name
    :return: Wrapped User with HTML mention
    """
    return f'<a href="tg://user?id={user_id}">{full_name}</a>'


def build_menu(buttons: List[str or InlineKeyboardButton],
               n_cols=None,
               header_buttons=None,
               footer_buttons=None) -> List[List[str]]:
    """ Building Menu with given arguments

    :param buttons: List of buttons,
    :param n_cols: button columns  ( maximum - 8 )
    :param header_buttons: Buttons to place on top
    :param footer_buttons: Buttons to place on bottom
    :return: List of
    """
    if not n_cols:
        n_cols = Config().inline_keyboard_max_cols
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu


def reply_markup_keyboard(keyboard) -> ReplyKeyboardMarkup:
    """ Shortcut for ReplyKeyboardMarkup creation

    :param keyboard: List of List of keyboard buttons
        ex: [ [ 'button', 'button ], [ 'button' ] ]
        will be:
        [ button ] [ button ]
        [       button      ]
    :return: ReplyKeyboardMarkup object
    """
    return ReplyKeyboardMarkup(keyboard=keyboard, resize_keyboard=True)


def inline_ticket_keyboard(ticket_id: str, keyboard):
    """ Shortcut for ticket InlineKeyboard creation

    :param ticket_id: Arbitrary Ticket ID
    :param keyboard: List of List of keyboard buttons
        ex: [ [ 'button', 'button ], [ 'button' ] ]
        will be:
        [ button ] [ button ]
        [       button      ]
    :return: InlineKeyboardMarkup object
    """
    return InlineKeyboardMarkup([
        [InlineKeyboardButton(button, callback_data=f'{ticket_id} {button}')
         for btn in keyboard for button in btn]
    ])


def ticket_list_message(tickets: list, for_support: bool) -> Tuple[str, int]:
    """ Formats given tickets list to string

    :param tickets: Tickets to format
    :param for_support: whether to add to each ticket mention of User created specified ticket
    :return: Tuple of formatted tickets
    and number of tickets listed in this message, used to overpass limitations of telegram message length
    """
    from source.model.entities import TicketStatus
    text = ''
    ticket_num = 0
    for index, ticket in enumerate(tickets[:Config().ticket_message_maxlen]):
        mention = user_mention(ticket.created_by.id, ticket.created_by.full_name) + '\n'
        if_postponed = (
            f'на {format_datetime(ticket.postponed_by_date, Config().pretty_month, locale=Config().main_locale)}'
            if ticket.status == TicketStatus.postponed else ''
        )
        three_dots_needed = '...' if len(ticket.description.split()[:10]) > 10 else ''
        description = " ".join(ticket.description.split()[:10]) + three_dots_needed
        buffer = (
                f'<b>{index + 1}</b>. '
                f'{description} '
                f'<b>{ticket.status} {if_postponed}</b> ' + '\n'
        )
        buffer += mention if for_support else ''
        if len(text) + len(buffer) < Config().message_maxlen:
            text += buffer
            ticket_num += 1
        else:
            break
    return text, ticket_num
