import re

from source.config import Config


def is_valid_name(string: str) -> bool:
    keywords = [v for k, v in Config().assets.data.items()]
    if string in keywords:
        return False
    pattern = Config().cyrillic_name_match
    if len(string.split(' ')) >= 2:
        name, surname, *_ = string.split(' ')
        if not _:
            if re.fullmatch(pattern, name) and re.fullmatch(pattern, surname):
                is_name = re.findall(pattern, name)[0] == name
                is_sur = re.findall(pattern, surname)[0] == surname
                return is_name and is_sur
    return False


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
