import functools
import logging
from pprint import pformat

from source.config import Config


def get_logger(name=None, level=None, root=False):
    logging.basicConfig(format=Config().logging_format, datefmt=Config().datefmt)
    logger = logging.getLogger(name if name else None if root else __name__)
    logger.setLevel(level if level else Config().logging_level)
    return logger


def log(func):
    @functools.wraps(func)
    def decorator(*args, **kwargs):
        logger = get_logger(func.__module__, logging.DEBUG)
        logger.debug('Entering: %s', func.__name__)
        result = func(*args, **kwargs)
        logger.debug(pformat(result))
        logger.debug('Exiting: %s', func.__name__)
        return result

    return decorator
