import locale
import logging
from collections import namedtuple
from os import environ
from urllib.parse import urlparse

from peewee import PostgresqlDatabase
from playhouse.pool import PooledPostgresqlDatabase
from telegram import MAX_MESSAGE_LENGTH
from telegram.ext.dispatcher import DEFAULT_GROUP

from source.text_assets import TextAssets

database = namedtuple('database', ['host', 'database', 'user', 'password'])
SMTP_server = namedtuple('smtpserver', ['address', 'port'])
Email = namedtuple('email', ['username', 'password'])


class Config(object):
    __instance = None

    class __Config:
        def __init__(self):
            """ Config container
            """
            self.default_encoding = 'UTF-8'
            self.ukr_locale = 'uk_UA.UTF-8'
            self.locale = locale.setlocale(locale.LC_ALL, 'uk_UA.UTF-8')
            self.main_locale = 'uk_ua'

            # text assets
            self.text_assets_file_path = 'text/texts.json'
            self.assets = TextAssets(self.text_assets_file_path)

            # logging
            self.logging_level = logging.DEBUG
            self.logging_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

            # date format
            self.datefmt = '[%H:%M:%S %d/%m/%y]'
            self.pretty_datetime = '%H:%M:%S %d/%m/%y'
            self.pretty_date = '%d/%m/%y'
            self.pretty_month = 'dd MMMM yyyy'
            self.xlsx_date = '%H-%M-%S_%d-%m-%y.xlsx'
            self.database = PostgresqlDatabase(**self.db_url_parse(environ.get('DATABASE_URL')))
            self.pooled_database = PooledPostgresqlDatabase(**self.db_url_parse(environ.get('DATABASE_URL')))
            self.smtp_server = SMTP_server(address=environ.get('smtp_server'),
                                           port=environ.get('smtp_port'))
            self.email = Email(username=environ.get('email_user'),
                               password=environ.get('email_pass'))

            # bot/network
            self.bot_token = environ.get('bot_token')
            self.webhook = environ.get('webhook')
            self.localhost = '0.0.0.0'
            self.port = int(environ.get('PORT') or 0)

            # server network data
            self.conn_pool_size = 35
            self.read_timeout = 10
            self.workers = 30
            self.msg_queue_burst_limit = 28
            self.msg_queue_burst_time_limit = 999

            # bot message handling
            self.evris_group_id = int(environ.get('evris_group_id'))
            self.tech_admin_id = int(environ.get('tech_admin_id'))
            self.default_group = DEFAULT_GROUP
            self.all_message = -1
            self.message_maxlen = MAX_MESSAGE_LENGTH - 1
            self.ticket_message_maxlen = 30
            self.inline_keyboard_max_cols = 8

            # cache
            self.cache_user_access_time = 10
            self.cache_interceptor_time = .5

            # regexps
            self.cyrillic_name_match = r"^[а-яА-ЯіІїЇєЄЪъЫыЭэЁё`']{4,20}"
            self.easter_pattern = 'хохо'

            # misc
            self.flood_pics_time = .9
            self.flood_message_time = .25
            self.animal_api_fetch_map = {
                'https://randomfox.ca/floof/': 'image',
                'https://dog.ceo/api/breeds/image/random': 'message',
                'https://api.thecatapi.com/v1/images/search': 'url',
            }
            self.phrases_url = 'https://theytoldme.com'
            self.cocktails_api = 'https://www.thecocktaildb.com/api/json/v1/1/random.php'

        def db_url_parse(self, db_url):
            parsed = urlparse(db_url)
            return {
                'host': parsed.hostname,
                'database': parsed.path.strip('/'),
                'user': parsed.username,
                'password': parsed.password,
            }

    def __new__(cls):
        if Config.__instance is None:
            Config.__instance = Config.__Config()
        return Config.__instance

    def __getattr__(self, name):
        return getattr(self.__instance, name)
