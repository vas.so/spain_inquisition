import json


class Endpoint:
    def __init__(self, name, text, keyboard):
        """  Shortcut for Endpoint objects
        :param name: Name of endpoint
        :param text: endpoint text
        :param keyboard: keyboard of endpoint
        """
        self.name = name
        self.text = text
        self.keyboard = keyboard


class TextAssets:
    def __init__(self, data_file_path):
        """ Object to store text assets of bot

        :param data_file_path: Path of file with dialogs
        """
        self.data_file_path = data_file_path
        self.data = self.load_text()

        self.on_custom_text = self.data['on_custom_text']
        self.name_not_provided = self.data['name_not_provided']
        self.user_not_authorized = self.data['user_not_authorized']
        self.user_access_state = self.data['user_access_state']
        self.only_future_dates = self.data['only_future_dates']
        self.incorrect_date_provided = self.data['incorrect_date_provided']
        self.rename_hint = self.data['rename_hint']
        self.privilegies_hint = self.data['privilegies_hint']
        self.no_tickets_found = self.data['no_tickets_found']
        self.there_is_no_tickets = self.data['there_is_no_tickets']
        self.ticket_is_closed = self.data['ticket_is_closed']

        self.ticket_status_columns = self.data['ticket_status_columns']

        self.tickets_statuses = self.data['tickets_statuses']
        self.ticket_postponed = self.tickets_statuses['postponed']
        self.ticket_declined = self.tickets_statuses['declined']
        self.ticket_done = self.tickets_statuses['done']
        self.ticket_pending = self.tickets_statuses['pending']

        self.this_day = self.data['this_day']
        self.yesterday = self.data['yesterday']
        self.this_week = self.data['this_week']
        self.this_month = self.data['this_month']
        self.custom = self.data['custom']

        self.ticket_created = self.data['ticket_created']
        self.operation_successful = self.data['operation_successful']

        self.create_ticket = self.data['create_ticket']
        self.ticket_list = self.data['ticket_list']
        self.do_report = self.data['do_report']
        self.cancel = self.data['cancel']
        self.im_bored = self.data['im_bored']
        self.flood_pics_message = self.data['flood_pics_message']

        self.endp_cancel = Endpoint(**self.__get_endpoint('cancel'))
        self.endp_user_start = Endpoint(**self.__get_endpoint('user_start'))
        self.endp_manager_start = Endpoint(**self.__get_endpoint('manager_start'))
        self.endp_support_start = Endpoint(**self.__get_endpoint('support_start'))
        self.endp_create_ticket = Endpoint(**self.__get_endpoint('create_ticket'))
        self.endp_update_ticket_status = Endpoint(**self.__get_endpoint('update_ticket_status'))
        self.endp_update_comment = Endpoint(**self.__get_endpoint('update_comment'))
        self.endp_do_report = Endpoint(**self.__get_endpoint('do_report'))
        self.endp_custom_report = Endpoint(**self.__get_endpoint('custom_report'))
        self.endp_postpone_date = Endpoint(**self.__get_endpoint('postpone_date'))

        self.start_for = lambda role_start: self.__start_for(role_start)

    def load_text(self):
        """ Shortcut for data loading from file

        :return: Read data from file
        """
        with open(self.data_file_path) as file:
            data = json.load(file)
            return data

    def __start_for(self, role_start):
        """ Shortcut mapping for start endpoint for different roles

        :param role_start: user role start name
        :return: Arbitrary endpoint object
        """
        return {
            'user_start': self.endp_user_start,
            'manager_start': self.endp_manager_start,
            'support_start': self.endp_support_start
        }.get(role_start, self.endp_user_start)

    def __get_endpoint(self, name):
        """ Shortcut for extracting endpoint object from file data

        :param name: name of endpoint
        :return: Endpoint object
        """
        dct = self.data['endpoint'][name]
        dct.update({'name': name})
        return dct
