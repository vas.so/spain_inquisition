from telegram import Chat
from telegram.ext import BaseFilter

from source.telegram_bot.caching import is_user, is_manager, is_support, get_user


def only_user_role(user_id: int) -> bool:
    """ Checks if User by given ID has only user role
    :param user_id: User ID
    :return: True if exists. active and has only user role, else - False
    """
    return (
        is_user(user_id)
        and not any([
            is_manager(user_id),
            is_support(user_id)
        ])
    )


def only_manager_role(user_id: int) -> bool:
    """ Checks if User by given ID has only manager role
    :param user_id: User ID
    :return: True if exists. active and has only manager role, else - False
    """
    return (
        is_user(user_id)
        and is_manager(user_id)
        and not is_support(user_id)
    )


def only_support_role(user_id: int) -> bool:
    """ Checks if User by given ID has only support role
    :param user_id: User ID
    :return: True if exists. active and has only support role, else - False
    """
    return (
        is_user(user_id)
        and is_support(user_id)
        and not is_manager(user_id)
    )


class ServiceFilter(BaseFilter):
    """ Class of Application bot filters
    """
    def filter(self, message):
        return False

    class _UserPrivate(BaseFilter):
        name = 'Service.user'

        def filter(self, message):
            """ Message must be private message
            :param message: Corresponding Message object
            :return: True if message is private, else False
            """
            if message.chat.type == Chat.PRIVATE:
                return is_user(message.from_user.id)
            return False

    class _ManagerPrivate(BaseFilter):
        name = 'Service.manager'

        def filter(self, message):
            """ Message must be private message and User must be manager
            :param message: Corresponding Message object
            :return: True if message is private and user is manager, else False
            """
            if message.chat.type == Chat.PRIVATE:
                if ServiceFilter.has_name.filter(message):
                    return only_manager_role(message.from_user.id)
            return False

    class _SupportPrivate(BaseFilter):
        name = 'Service.support'

        def filter(self, message):
            """ Message must be private message and user must be support
            :param message: Corresponding Message object
            :return: True if message is private and user is support, else False
            """
            if message.chat.type == Chat.PRIVATE:
                if ServiceFilter.has_name.filter(message):
                    return only_support_role(message.from_user.id)
            return False

    class _HasName(BaseFilter):
        name = 'Service.client.has name'

        def filter(self, message):
            """ User must have his name written
            :param message: Corresponding Message object
            :return: True if User has real name, else False
            """
            if message.chat.type == Chat.PRIVATE:
                user = get_user(message.from_user.id)
                if user:
                    return user.has_real_name
            return False

    class _HasNotName(BaseFilter):
        name = 'Service.client.has not name'

        def filter(self, message):
            """ Inverted to _HasName User must have his name written
            :param message: Corresponding Message object
            :return: True if User has real name, else False
            """
            if message.chat.type == Chat.PRIVATE:
                user = get_user(message.from_user.id)
                if user:
                    return not user.has_real_name
            return False

    user = _UserPrivate()
    manager = _ManagerPrivate()
    support = _SupportPrivate()
    has_name = _HasName()
    has_not_name = _HasNotName()
