from typing import List

from source.config import Config
from source.model.entities import ServiceUser
from source.utils import MWT


@MWT(Config().cache_interceptor_time)
def active_users():
    """ Retreiving list of service users
    :return: List of active service users
    """
    return [user for user in ServiceUser.get_all()]


@MWT()
def active_user_ids():
    """ Retreiving list of service users IDs
    :return: List of active service users IDs
    """
    return [user.id for user in active_users()]


@MWT(Config().cache_user_access_time)
def active_users_to_html():
    """ Retreiving list of service users wrapped in HTML
    :return: List of active service users wrapped in HTML
    """
    return [user.to_html() for user in active_users()]


@MWT(Config().cache_user_access_time)
def is_active(user_id: int) -> bool:
    """ Check if given user_id is active

    :param user_id: User id to check
    :return: true if active, else - false
    """
    user = ServiceUser.get_by_id(user_id)
    if user:
        return user.is_active
    return False


@MWT(Config().cache_user_access_time)
def is_user(user_id: int) -> bool:
    """ Check if given user id is user and is active
    :return: true if exists and is_active, else - false
    """
    return is_active(user_id)


@MWT(Config().cache_user_access_time)
def is_manager(user_id: int) -> bool:
    """ Check if given user id is manager and is active
    :return: true if manager and is_active, else - false
    """
    user = get_user(user_id)
    if user:
        return user.is_active and user.is_manager
    return False


@MWT(Config().cache_user_access_time)
def is_support(user_id: int) -> bool:
    """ Check if given user id is support and is active
    :return: true if support and is_active, else - false
    """
    user = get_user(user_id)
    if user:
        return user.is_active and user.is_support
    return False


@MWT(Config().cache_user_access_time)
def get_user(user_id) -> ServiceUser:
    """ Retreiving User object by his ID

    :param user_id: User ID to search in database
    :return: Arbitrary User object or None if not exist
    """
    return ServiceUser.get_by_id(user_id)


@MWT(Config().cache_user_access_time)
def get_supports() -> List[ServiceUser]:
    """ Retreiving list of supports

    :return: List of supports
    """
    return ServiceUser.get_supports()
