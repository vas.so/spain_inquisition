import os
from datetime import date
from datetime import datetime
import random
from typing import Callable, Tuple, Set

from telegram import (
    Bot, Update, ParseMode,
    InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardRemove,
    Message, ChatAction)
from telegram.error import Unauthorized, BadRequest, TimedOut, NetworkError, ChatMigrated, TelegramError
from telegram.ext import MessageHandler, Filters, Handler, CommandHandler, ConversationHandler, CallbackQueryHandler

from source.config import Config
from source.model.datetime_utils import date_convert, Datetime
from source.model.entities import Ticket, TicketStatus, ServiceUser
from source.model.report import Report, to_xlsx
from source.telegram_bot.caching import get_user, is_support, is_user, is_manager, get_supports, active_user_ids, \
    active_users_to_html
from source.telegram_bot.filters import ServiceFilter
from source.telegram_bot.states import get_role, ConversationStates
from source.utils import (
    build_menu, ticket_list_message, user_chat_message, reply_markup_keyboard,
    user_mention, get_logger, is_valid_name, random_animal_photo, chunks, log)
from source.utils.bot_utils import inline_ticket_keyboard, random_phrase, random_cocktail


class UnauthorizedUser(TelegramError):
    pass


class BotHandler(object):

    @staticmethod
    def allow_user_interceptor(bot: Bot, update: Update):
        """ Decides whether to allow user to access bots functionality

        If user is not allowed - raises exception which prevents other handlers to process this update
        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        if not (update.effective_user.id in active_user_ids()):
            update.message.reply_text(Config().assets.user_not_authorized)
            raise UnauthorizedUser('User is tried to use this bot, intercepted update - ')

    @staticmethod
    def easter_handler(bot: Bot, update: Update, user_data: dict):
        """ Secret handler invokes when pattern matches

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        """
        user, chat, message = user_chat_message(update)
        message.reply_text(random_phrase())

    @classmethod
    def choice(cls, choice) -> Callable:
        """ Decides which method must process update

        :param choice: text user wrote or button user pressed
        :return: Correspondent handler or start handler
        """
        return {
            Config().easter_pattern: cls.easter_handler,
            Config().assets.im_bored: cls.im_bored,
            Config().assets.create_ticket: cls.create_ticket,
            Config().assets.do_report: cls.do_report,
            Config().assets.ticket_list: cls.ticket_list
        }.get(choice, cls.on_custom_text)

    @staticmethod
    def on_custom_text(bot: Bot, update: Update, user_data: dict):
        """ On custom text handler

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        """
        user, chat, message = user_chat_message(update)
        keyboard = Config().assets.start_for(get_role(user.id).start).keyboard
        message.reply_text(random.choice(Config().assets.on_custom_text),
                           parse_mode=ParseMode.HTML,
                           reply_markup=reply_markup_keyboard(keyboard))

    @staticmethod
    def top_handler(bot: Bot, update: Update, user_data: dict):
        """ First method to decide how to process update if not in conversation

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: Callable which must process current update
        """
        user, chat, message = user_chat_message(update)
        if BotHandler.choice(message.text):
            return BotHandler.choice(message.text)(bot, update, user_data)

    @staticmethod
    def start(bot: Bot, update: Update, user_data: dict):
        """ Start of conversation handler, also invokes when someone writes /start to bot

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: -1 End of conversation
            look on telegram.ext.ConversationHandler for details
        """
        user, chat, message = user_chat_message(update)
        role = get_role(user.id)
        if role:
            model_user = get_user(user.id)
            if model_user.has_real_name:
                endpoint = Config().assets.start_for(role.start)
                text, keyboard = '\n'.join(endpoint.text), endpoint.keyboard
                message.reply_text(text,
                                   reply_markup=reply_markup_keyboard(keyboard))
            else:
                message.reply_text(Config().assets.name_not_provided, reply_markup=ReplyKeyboardRemove())
            return ConversationStates.end

    @staticmethod
    def im_bored(bot: Bot, update: Update, user_data: dict):
        """ Sending random pic of animal

         :param bot: Telegram Bot object
         :param update: Incoming update
         :param user_data: arbitrary user_data
         :return: -1 End of conversation
             look on telegram.ext.ConversationHandler for details
         """
        user, chat, message = user_chat_message(update)
        role = get_role(user.id)
        if role:
            model_user = get_user(user.id)
            if model_user.has_real_name:
                if 'flood_timestamp' in user_data:
                    flood_timestamp = user_data['flood_timestamp']
                    seconds_passed = (Datetime.now() - flood_timestamp).total_seconds()
                    is_flood_stamp_passed = seconds_passed <= Config().flood_pics_time
                    if is_flood_stamp_passed:
                        message.reply_text(Config().assets.flood_pics_message)
                        return ConversationStates.end
                keyboard = reply_markup_keyboard(Config().assets.start_for(role.start).keyboard)
                choice = random.choice(range(2 if Datetime.now().isoweekday() == Datetime.Friday else 1))
                if choice == 0:
                    bot.send_chat_action(user.id, ChatAction.UPLOAD_PHOTO)
                    photo = random_animal_photo()
                    message.reply_photo(photo=photo, reply_markup=keyboard)
                elif choice == 1:
                    bot.send_chat_action(user.id, ChatAction.UPLOAD_PHOTO)
                    photo, text = random_cocktail()
                    message.reply_photo(photo=photo, reply_markup=keyboard)
                    message.reply_text(text, parse_mode=ParseMode.HTML)
            else:
                message.reply_text(Config().assets.name_not_provided, reply_markup=ReplyKeyboardRemove())
            user_data['flood_timestamp'] = Datetime.now()
            return ConversationStates.end

    @staticmethod
    def update_user_name(bot: Bot, update: Update):
        """ Updates user first and last names

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        user, chat, message = user_chat_message(update)
        user = get_user(user.id)
        if user:
            if is_valid_name(message.text):
                user.update_name(*message.text.split(' '))
                role = get_role(user.id)
                reply_markup = reply_markup_keyboard(Config().assets.start_for(role.start).keyboard)
                update.message.reply_text(f'Привіт, {message.text}', reply_markup=reply_markup)
            else:
                update.message.reply_text(Config().assets.name_not_provided, reply_markup=ReplyKeyboardRemove())

    @staticmethod
    @log
    def new_user(bot: Bot, update: Update):
        """ Add new user to database when someone joins specified group

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        if update.effective_user.is_bot:
            return
        for new_user in update.message.new_chat_members:
            user = ServiceUser.exists_get(new_user.id)
            if user:
                user.make_active()
            else:
                ServiceUser.add(new_user)

    @staticmethod
    @log
    def left_user(bot: Bot, update: Update):
        """ Removing user from database when he left the specified group

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        if update.message.left_chat_member:
            user = ServiceUser.exists_get(update.message.left_chat_member.id)
            if user:
                user.make_inactive()

    @staticmethod
    @log
    def create_ticket(bot: Bot, update: Update, user_data: dict):
        """ Starting of creating ticket

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: ticket_description state to ensure specified ticket using on next message
        """
        user, chat, message = user_chat_message(update)
        role, model_user = get_role(user.id), get_user(user.id)
        if role:
            if model_user and model_user.has_real_name:
                user, chat, message = user_chat_message(update)
                endpoint = Config().assets.endp_create_ticket
                text, keyboard = '\n'.join(endpoint.text), endpoint.keyboard
                message.reply_text(text, reply_markup=reply_markup_keyboard(keyboard))
                return ConversationStates.ticket_description

    @staticmethod
    @log
    def ticket_description(bot: Bot, update: Update, user_data: dict):
        """ Writes new ticket data to database, notify support about it

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: End of conversation state
        """
        user, chat, message = user_chat_message(update)
        role = get_role(user.id)
        if role:
            model_user = get_user(user.id)
            if message.text == Config().assets.cancel:
                BotHandler.start(bot, update, user_data)
            elif model_user.has_real_name:
                if message.photo:
                    [message.forward(support.id) for support in get_supports()]
                ticket_description = message.caption if message.caption else message.text
                keyboard = reply_markup_keyboard(Config().assets.start_for(role.start).keyboard)
                ticket = Ticket.new(ticket_description, user.id, message.date)
                text = f'{Config().assets.ticket_created}'
                message.reply_text(text, reply_markup=keyboard)
                BotHandler.notify_supports(bot, ticket)
                BotHandler.start(bot, update, user_data)
            return ConversationStates.end

    @staticmethod
    @log
    def notify_supports(bot: Bot, ticket: Ticket, with_keyboard: bool = True):
        """ Method for supports notifying about new ticket

        :param with_keyboard: whether to send message with keyboard or not
        :param bot: Telegram Bot object
        :param ticket: Created ticket
        """
        supports = get_supports()
        is_not_working_hours = not Datetime.is_working_hours()
        if supports and ticket:
            keyboard = None
            if with_keyboard:
                keyboard = Config().assets.endp_update_ticket_status.keyboard
                keyboard = inline_ticket_keyboard(ticket.uuid_short, keyboard)
            [bot.send_message(chat_id=support.id,
                              text=ticket.to_html(user_mention(ticket.created_by.id, ticket.created_by.full_name)),
                              reply_markup=keyboard,
                              parse_mode=ParseMode.HTML,
                              disable_notification=is_not_working_hours)
             for support in supports]

    @staticmethod
    @log
    def ticket_postpone_date(bot: Bot, update: Update, user_data: dict):
        """ Checking on support input on what time to postpone ticket, writes data to database if everything is ok

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return:
            - end of conversation if cancelled or applied
            - state for this handler if incorrect date was put
        """
        user, chat, message = user_chat_message(update)
        role = get_role(user.id)
        if role:
            model_user = get_user(user.id)
            if message.text == Config().assets.cancel:
                user_data['message_id'] = None
                BotHandler.start(bot, update, user_data)
                return ConversationStates.end
            elif model_user.has_real_name and user_data:
                try:
                    if message.text == '0':
                        by_date = Datetime.tomorrow()
                    else:
                        by_date = date_convert(message.text)
                        delta = by_date - date.today()
                        if delta.days < 0:
                            message.reply_text(Config().assets.only_future_dates)
                            text = '\n'.join(Config().assets.endp_postpone_date.text)
                            keyboard = Config().assets.endp_update_ticket_status.keyboard
                            message.reply_text(text, parse_mode=ParseMode.HTML,
                                               reply_markup=reply_markup_keyboard(keyboard))
                            return ConversationStates.ticket_postpone_date
                except ValueError as e:
                    get_logger().error(f'{e} - {update}')
                    keyboard = Config().assets.endp_cancel.keyboard
                    message.reply_text(Config().assets.incorrect_date_provided,
                                       reply_markup=reply_markup_keyboard(keyboard))
                    return ConversationStates.ticket_postpone_date
                else:
                    ticket_id, description = user_data['ticket_id'], user_data['postpone_desc']
                    ticket = Ticket.get_and_postpone(ticket_id, description, by_date)
                    message_text = (
                            f'{ticket.description}' + '\n' +
                            f'<b>{ticket.status}</b> на {by_date.strftime(Config().pretty_date)}, '
                            f'<b>коментар:</b> {description}'
                    )
                    bot.send_message(ticket.created_by.id, message_text, parse_mode=ParseMode.HTML)
                    BotHandler.start(bot, update, user_data)
                    BotHandler.notify_supports(bot, ticket, with_keyboard=False)
            return ConversationStates.end

    @staticmethod
    @log
    def ticket_status_description(bot: Bot, update: Update, user_data: dict):
        """ Handle ticket status update comments from supports

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return:
            - end of conversation if cancelled or success
            - postpone state to specify date
        """
        user, chat, message = user_chat_message(update)
        role = get_role(user.id)
        if role:
            model_user = get_user(user.id)
            if message.text == Config().assets.cancel:
                user_data['message_id'] = None
                BotHandler.start(bot, update, user_data)
                return ConversationStates.end
            elif model_user.has_real_name and user_data:
                ticket_id, ticket_status = user_data['ticket_id'], user_data['ticket_status']
                ticket_status = ticket_status if ticket_status.isalpha() else ticket_status[1:]
                if ticket_status == TicketStatus.postponed:
                    text = '\n'.join(Config().assets.endp_postpone_date.text)
                    by_time = Datetime.from_days_or_time(days=2)
                    text = text.replace('@', by_time.strftime(Config().pretty_date))
                    user_data['postpone_desc'] = message.text
                    keyboard = reply_markup_keyboard(Config().assets.endp_cancel.keyboard)
                    message.reply_text(text, parse_mode=ParseMode.HTML, reply_markup=keyboard)
                    return ConversationStates.ticket_postpone_date
                else:
                    method = TicketStatus.get_and_(ticket_status)
                    if not method:
                        return ConversationStates.end
                    ticket = method(ticket_id, message.text)
                    keyboard = reply_markup_keyboard(Config().assets.start_for(role.start).keyboard)
                    if 'message_id' in user_data:
                        bot.edit_message_reply_markup(chat.id, user_data['message_id'])
                        user_data['message_id'] = None
                    message.reply_text(Config().assets.operation_successful, reply_markup=keyboard)
                    message_text = (
                        '{}\n<b>{}</b>, <b>коментар:</b> {}'.format(ticket.description, ticket_status, message.text)
                    )
                    bot.send_message(ticket.created_by.id, message_text, parse_mode=ParseMode.HTML)
                    BotHandler.notify_supports(bot, ticket, with_keyboard=False)
                return ConversationStates.end

    @staticmethod
    def do_report(bot: Bot, update: Update, user_data: dict):
        """ Starting report creation

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: conversation state to choose report time
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            endpoint = Config().assets.endp_do_report
            text, keyboard = '\n'.join(endpoint.text), endpoint.keyboard
            message.reply_text(text, reply_markup=reply_markup_keyboard(keyboard))
            return ConversationStates.report

    @staticmethod
    def report_by_time(bot: Bot, update: Update, user_data: dict):
        """ Choosing time of report to make for

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return:
            - end of conversation if date range is not custom
            - custom date range state
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            user, chat, message = user_chat_message(update)
            keyboard = [string for row in Config().assets.endp_do_report.keyboard for string in row]
            if message.text in keyboard:
                if message.text == keyboard[-1]:
                    endpoint = Config().assets.endp_custom_report
                    text, keyboard = '\n'.join(endpoint.text), endpoint.keyboard
                    message.reply_text(text, reply_markup=reply_markup_keyboard(keyboard))
                    return ConversationStates.custom_report
                else:
                    BotHandler.prepare_and_send_report(message)
            BotHandler.start(bot, update, user_data)
            return ConversationHandler.END

    @staticmethod
    @log
    def custom_report(bot: Bot, update: Update, user_data: dict):
        """ Making custom date range report

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return:
            - end of conversation if everything is ok
                or canceled
            - custom_report state if incorrect data provided
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            if message.text == Config().assets.cancel:
                BotHandler.start(bot, update, user_data)
                return ConversationStates.end
            from_date, to_date = '', ''
            try:
                from_date, to_date = message.text.split(' ')
            except ValueError as e:
                message.reply_text(Config().assets.incorrect_date_provided)
                return ConversationStates.custom_report
            else:
                try:
                    from_date = date_convert(from_date)
                    to_date = date_convert(to_date)
                except Exception:
                    return ConversationStates.custom_report
                else:
                    BotHandler.prepare_and_send_report(message, from_date, to_date)
            BotHandler.start(bot, update, user_data)
            return ConversationStates.end

    @staticmethod
    @log
    def prepare_and_send_report(message: Message, from_date: datetime = None, to_date: datetime = None):
        method = Report.day_map(message.text)
        if from_date and to_date:
            from_date, to_date = (to_date, from_date) if to_date < from_date else (from_date, to_date)
        ticket_logs = method(from_date, to_date)
        report_file = to_xlsx(ticket_logs)
        if report_file:
            with open(report_file.filename, 'rb') as file:
                message.reply_document(file)
                os.remove(file.name)
        else:
            message.reply_text(Config().assets.no_tickets_found)

    @staticmethod
    def ticket_list(bot: Bot, update: Update, user_data: dict):
        """ Sending tickets list
            if user -> all opened ticket by this user
            if support -> all opened ticket

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        """
        user, chat, message = user_chat_message(update)
        if message.text == Config().assets.cancel:
            user_data['message_id'] = None
            BotHandler.start(bot, update, user_data)
        if is_user(user.id):
            user_is_support = is_support(user.id)
            if user_is_support:
                tickets = Ticket.get_all_opened()
            else:
                tickets = Ticket.get_all_opened_by_userid(user.id)
            text, ticket_num = ticket_list_message(tickets, for_support=user_is_support)
            if ticket_num:
                if user_is_support:
                    text += f'Усього: {len(tickets)}'
                    buttons = [InlineKeyboardButton(ind + 1, callback_data=ticket.uuid_short)
                               for ind, ticket in enumerate(tickets[:ticket_num])]
                    keyboard = InlineKeyboardMarkup(build_menu(buttons))
                    message.reply_text(text, reply_markup=keyboard, parse_mode=ParseMode.HTML)
                else:
                    keyboard = Config().assets.start_for(get_role(user.id).start).keyboard
                    message.reply_text(text,
                                       reply_markup=reply_markup_keyboard(keyboard),
                                       parse_mode=ParseMode.HTML)
            else:
                message.reply_text(Config().assets.there_is_no_tickets)
        return ConversationStates.end

    @staticmethod
    def ticket_inline_query(bot: Bot, update: Update, user_data: dict):
        """ Sending messages with ticket description and handling ticket status change

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return:
            - ticket_status_description state if status is changing
            - end of conversation - else
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            data = update.callback_query.data
            if len(data.split()) > 1:
                user_data['ticket_id'], user_data['ticket_status'] = data.split()
                if Ticket.get_by_uuid(user_data['ticket_id']).status in TicketStatus.closed:
                    message.reply_text(Config().assets.ticket_is_closed)
                    return ConversationStates.end
                endpoint = Config().assets.endp_update_comment
                text, keyboard = '\n'.join(endpoint.text), endpoint.keyboard
                message.reply_text(text, reply_markup=reply_markup_keyboard(keyboard))
                return ConversationStates.ticket_status_description
            else:
                if Ticket.get_by_uuid(data).status in TicketStatus.closed:
                    message.reply_text(Config().assets.ticket_is_closed)
                    return ConversationStates.end
                ticket = Ticket.get_by_uuid(data)
                keyboard = Config().assets.endp_update_ticket_status.keyboard
                keyboard = inline_ticket_keyboard(ticket.uuid_short, keyboard)
                sent_promise = message.reply_text(
                    ticket.to_html(user_mention(user.id, ticket.created_by.full_name)),
                    reply_markup=keyboard,
                    parse_mode=ParseMode.HTML)
                user_data['message_id'] = sent_promise.result().message_id
                return ConversationStates.end

    @staticmethod
    @log
    def rename_user(bot: Bot, update: Update):
        """ Renaming user

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            try:
                _, user_id, fname, lname = message.text.split(' ')
            except ValueError:
                message.reply_text(Config().assets.rename_hint)
            else:
                user = get_user(user_id)
                if user:
                    user.update_name(fname, lname)
                    message.reply_text(f'{user_id} {fname} {lname} - Успішно змінено.')

    @staticmethod
    @log
    def privilegies_change(bot: Bot, update: Update):
        """ Change user privilegies

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        user, chat, message = user_chat_message(update)
        try:
            _, user_id, priv = message.text.split(' ')
        except ValueError:
            message.reply_text(Config().assets.privilegies_hint)
        else:
            user = get_user(user_id)
            if user:
                user.set_privilegies(**Config().assets.user_access_state.get(priv))

    @staticmethod
    def user_list(bot: Bot, update: Update):
        """ Sending users list

        :param bot: Telegram Bot object
        :param update: Incoming update
        """
        user, chat, message = user_chat_message(update)
        if is_support(user.id) or is_manager(user.id):
            users = active_users_to_html()
            chunk_size = 30
            for user_chunk in chunks(users, chunk_size):
                message.reply_text('\n'.join(user_chunk), parse_mode=ParseMode.HTML)

    @staticmethod
    def cancel(bot: Bot, update: Update, user_data: dict):
        """ Canceling current operation

        :param bot: Telegram Bot object
        :param update: Incoming update
        :param user_data: arbitrary user_data
        :return: end of conversation
        """
        user, chat, message = user_chat_message(update)
        if is_user(user.id) and message.text == Config().assets.cancel:
            BotHandler.start(bot, update, user_data)
            return ConversationStates.end

    @classmethod
    def handlers(cls) -> Set[Tuple[int, Handler]]:
        """ Initialization all of bot handlers

        :return: Collection of bot handlers and its group identifier
        """
        interceptor = MessageHandler(Filters.all, cls.allow_user_interceptor)
        rename_user = CommandHandler('rename', cls.rename_user, ServiceFilter.support & ServiceFilter.has_name)
        user_list = CommandHandler('users', cls.user_list, ServiceFilter.support & ServiceFilter.has_name)
        start = CommandHandler(command='start',
                               callback=cls.start,
                               filters=ServiceFilter.has_not_name | ServiceFilter.user,
                               pass_user_data=True)
        privilegies_change = CommandHandler(command='priv',
                                            callback=cls.privilegies_change,
                                            filters=Filters.user(Config().tech_admin_id))
        name_not_provided = MessageHandler(filters=ServiceFilter.has_not_name & ServiceFilter.user,
                                           callback=cls.update_user_name)
        on_new_user = MessageHandler(Filters.status_update.new_chat_members, cls.new_user)
        on_left_user = MessageHandler(Filters.status_update.left_chat_member, cls.left_user)
        inner_conversation = ConversationHandler(
            entry_points=[
                MessageHandler(ServiceFilter.has_name & Filters.text, cls.top_handler, pass_user_data=True),
                CallbackQueryHandler(cls.ticket_inline_query, pass_user_data=True)
            ],
            states={
                ConversationStates.ticket_description: [
                    MessageHandler(Filters.text | Filters.photo,
                                   callback=cls.ticket_description,
                                   pass_user_data=True)
                ],
                ConversationStates.ticket_status_description: [
                    MessageHandler(Filters.text, cls.ticket_status_description, pass_user_data=True)
                ],
                ConversationStates.ticket_postpone_date: [
                    MessageHandler(Filters.text, cls.ticket_postpone_date, pass_user_data=True)
                ],
                ConversationStates.report: [
                    MessageHandler(Filters.text, cls.report_by_time, pass_user_data=True)
                ],
                ConversationStates.custom_report: [
                    MessageHandler(Filters.text, cls.custom_report, pass_user_data=True)
                ]
            },
            fallbacks=[]
        )
        return {
            (Config().default_group, privilegies_change),
            (Config().default_group, start),
            (Config().default_group, rename_user),
            (Config().default_group, user_list),
            (Config().default_group, inner_conversation),
            (Config().default_group, name_not_provided),
            (Config().all_message, interceptor),
            (Config().evris_group_id, on_new_user),
            (Config().evris_group_id, on_left_user),
        }

    @staticmethod
    def error_handler() -> Callable:
        """ Defines behaviour when specified exceptions raises

        :return: callable method to call when exception raises
        """
        def handler(bot: Bot, update: Update, error: Exception):
            """

            :param bot: Telegram Bot object
            :param update: Incoming update
            :param error: raised exception
            """
            try:
                raise error
            except UnauthorizedUser as exception:
                get_logger().error(f'{exception} - {update}')
            except Unauthorized as exception:
                get_logger().error(f'{exception} - {update}')
            except BadRequest as exception:
                get_logger().error(f'{exception} - {update}')
            except TimedOut as exception:
                get_logger().error(f'{exception} - {update}')
            except NetworkError as exception:
                get_logger().error(f'{exception} - {update}')
            except ChatMigrated as exception:
                get_logger().error(f'{exception} - {update}')
            except TelegramError as exception:
                get_logger().error(f'{exception} - {update}')
            except Exception as exception:
                get_logger().error(f'{exception} - {update}')
            finally:
                return ConversationStates.end
        return handler
