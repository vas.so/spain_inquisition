from typing import Tuple, Set

from telegram.ext import MessageQueue, Updater, Handler
from telegram.ext.dispatcher import DEFAULT_GROUP
from telegram.utils.request import Request

from source.config import Config
from source.model.entities import ServiceUser
from source.telegram_bot.bot_handler import BotHandler
from source.telegram_bot.message_queue_bot import MessageQueueBot
from source.utils import get_logger


class ApplicationBot(object):
    def __init__(self):
        """ Main Bot class

        config: Configuration object
        bot_handler: Bot handlers
        user_db: Helper object for interacting with User table
        logger: logger object

        # used for flood limit control
        msg_queue: Handling messages per second sending
        request: Handling connections

        # Bot
        bot: PTB Bot object, using for interacting with Telegram
        updater: Bot server object
        """
        self.config = Config()
        self.bot_handler = BotHandler()
        self.user_db = ServiceUser()
        self.logger = get_logger(level=self.config.logging_level, root=True)
        self.msg_queue = MessageQueue(all_burst_limit=self.config.msg_queue_burst_limit,
                                      all_time_limit_ms=self.config.msg_queue_burst_time_limit)
        self.request = Request(con_pool_size=self.config.conn_pool_size,
                               read_timeout=self.config.read_timeout)
        self.bot = MessageQueueBot(self.config.bot_token,
                                   request=self.request,
                                   mqueue=self.msg_queue)
        self.updater = Updater(bot=self.bot,
                               workers=self.config.workers)

    def add_handler(self, handler: Handler, group: int = DEFAULT_GROUP):
        """ Adding handler to dispatcher server

        :param handler: handler object inherited from Handler class
        :param group: which group should this handler handle
            more info -> telegram.Dispatcher.add_handler()
        """
        self.updater.dispatcher.add_handler(handler, group)

    def add_handlers(self, handlers: Set[Tuple[int, Handler]]):
        """ Adding collection of handlers to dispatcher server

        :param handlers: collection of handlers objects and group which handler should handler
        ex: (handler, group_number),
        """
        [self.add_handler(handler, group_id) for group_id, handler in handlers]

    def add_error_handler(self, handler):
        """ Adding error handler to dispatcher server

        :param handler: Error Handler object
        """
        self.updater.dispatcher.add_error_handler(handler)

    def start(self):
        """
        1. starting bot
        2. adding handlers and error handlers
        3. setting webhook if provided else polling
        """
        self.add_handlers(self.bot_handler.handlers())
        self.add_error_handler(self.bot_handler.error_handler())
        if self.config.webhook:
            self.updater.start_webhook(listen=self.config.localhost,
                                       port=self.config.port,
                                       url_path=self.config.bot_token)
            self.bot.set_webhook(self.config.webhook + self.config.bot_token)
        else:
            self.updater.start_polling()
            self.updater.idle()
