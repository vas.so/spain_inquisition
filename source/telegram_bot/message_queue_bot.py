import telegram
from telegram.ext import messagequeue

from source.config import Config


class MessageQueueBot(telegram.bot.Bot):
    """ This class is a wrapper for dispatcher which handle flood limit of telegram

        Attributes:
            is_queued_def: Boolean: Defines whether to use message queue
            mqueue: MessageQueue: Message queue object, if not passed a new object will be created
    """

    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(MessageQueueBot, self).__init__(*args, **kwargs)
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = (mqueue or messagequeue.MessageQueue(
            all_burst_limit=Config().msg_queue_burst_limit,
            all_time_limit_ms=Config().msg_queue_burst_time_limit
        ))

    def __del__(self):
        try:
            self._msg_queue.stop()
        except:
            pass
        super(MessageQueueBot, self).__del__()

    @messagequeue.queuedmessage
    def edit_message_reply_markup(self, *args, **kwargs):
        """ Shortcut for calling base method
        """
        return super(MessageQueueBot, self).edit_message_reply_markup(*args, **kwargs)

    @messagequeue.queuedmessage
    def edit_message_text(self, *args, **kwargs):
        """ Shortcut for calling base method
        """
        return super(MessageQueueBot, self).edit_message_text(*args, **kwargs)

    @messagequeue.queuedmessage
    def send_message(self, *args, **kwargs):
        """ Shortcut for calling base method
        """
        return super(MessageQueueBot, self).send_message(*args, **kwargs)

    @messagequeue.queuedmessage
    def answer_callback_query(self, *args, **kwargs):
        return super(MessageQueueBot, self).answer_callback_query(*args, **kwargs)

    @messagequeue.queuedmessage
    def delete_message(self, *args, **kwargs):
        """ Shortcut for calling base method
        """
        return super(MessageQueueBot, self).delete_message(*args, **kwargs)
