from telegram.ext import ConversationHandler

from source.telegram_bot.caching import is_user, is_support, is_manager


class ConversationStates:
    """ Conversation states stored in class for better readability
    """
    ticket_description, ticket_status_description, ticket_postpone_date, report, custom_report = range(5)
    end = ConversationHandler.END


class state:
    """ Security class for access to resources
    """
    access = ''
    name_not_provided = 'name_not_provided'


class user_state(state):
    """ Security User class for access to resources
    """
    access = 'user'
    start = f'{access}_start'


class manager_state(user_state):
    """ Security Manager class for access to resources
    """
    access = 'manager'
    start = f'{access}_start'


class support_state(user_state):
    """ Security Support class for access to resources
    """
    access = 'support'
    start = f'{access}_start'


def get_role(user_id: int):
    """ Identifying User role by his id
    :param user_id: User ID
    :return: Corresponding class or None if user_id not exist
    """
    if is_user(user_id):
        if is_support(user_id):
            return support_state
        elif is_manager(user_id):
            return manager_state
        else:
            return user_state
    else:
        return None
