from source.model.entities import ServiceUser
from source.model.entities import Ticket
from source.model.entities import TicketStatusLog
from source.model.entities import get_database


def migrate():
    database = get_database()
    with database.atomic():
        database.create_tables([
            Ticket, TicketStatusLog, ServiceUser
        ])


if __name__ == '__main__':
    migrate()
