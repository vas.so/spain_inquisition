from datetime import datetime
from typing import List

from peewee import DateTimeField, ForeignKeyField, CharField, TextField

from source.model.entities.database import DatabaseModel
from source.model.entities.ticket import Ticket


class TicketStatusLog(DatabaseModel):
    """ Represents ticket_status_log table
    """
    id = None
    ticket_id = ForeignKeyField(Ticket, Ticket.uuid_short, unique=False)
    status = CharField(index=True)
    status_description = TextField(null=True)
    date = DateTimeField(index=True)
    postponed_by_date = DateTimeField(index=True, null=True)

    @property
    def uuid_short(self):
        return self.ticket_id.uuid_short

    @property
    def description(self):
        return self.ticket_id.description

    @property
    def created_by(self):
        return self.ticket_id.created_by

    @property
    def created_at(self):
        return self.ticket_id.created_at

    @classmethod
    def get_by_id(cls, ticket_id) -> List[Ticket]:
        """ Getting tickets logs with given id
        :param ticket_id: given ticket id
        :return: list of tickets
        """
        return [ticket for ticket in cls.select().where(cls.ticket_id == ticket_id)]

    @staticmethod
    def new(ticket: Ticket, description: str or None, by_date: datetime = None):
        """ Create new record
        :param ticket: given Ticket object
        :param description: status change description or None if just created
        :param by_date: if ticket was postponed, indicates by what date to postpone
        :return: TicketStatusLog object created
        """
        return TicketStatusLog.create(ticket_id=ticket.uuid_short,
                                      status=ticket.status,
                                      status_description=description,
                                      postponed_by_date=by_date,
                                      date=ticket.last_activity)

    def __repr__(self):
        return (
            f'ticket_id: {self.ticket_id}, '
            f'status: {self.status}, '
            f'status_description: {self.status_description}, '
            f'date: {self.date}'
            f'postponed_by_date: {self.postponed_by_date}'
        )

    def __str__(self):
        return repr(self)
