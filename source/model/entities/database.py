import operator
from functools import wraps

from peewee import Model, InterfaceError

from source.config import Config
from source.utils import get_logger


def with_connection(require_closing=True):
    """ Helper annotation used whenever a connection to database is needed to be closed
    Also provides default atomic transactions to every query executed

    :param require_closing: Whether connection should be closed or not
    :return: annotated function with arguments
    """
    def func_wrapper(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            database = get_database()
            result = None
            with database.atomic():
                try:
                    result = function(*args, **kwargs)
                except Exception as e:
                    get_database().execute_sql('rollback')
                    get_logger(__name__).error(str(e))
            if require_closing:
                try:
                    database.close()
                except InterfaceError as e:
                    get_logger(__name__).error(str(e))
                except Exception as e:
                    get_database().execute_sql('rollback')
                    get_logger(__name__).error(str(e))
            return result

        return wrapper
    return func_wrapper


def get_database():
    """ Helper function used to have a singleton object of database provided in config

    :return: peewee.Database child class specified in config
    """
    return Config().database


def get_pooled_database():
    """ Helper function used to have a singleton object of database provided in config

    :return: peewee.PooledDatabase child class specified in config
    """
    return Config().pooled_database


class DatabaseModel(Model):
    """ Helper database interactions class with shortcuts for often used methods

    """

    class Meta():
        database = get_database()

    @classmethod
    def select_with_operator(cls, field, value, operator_: callable = operator.eq):
        """ Select query and filter by given field *operator* value

        :param field: field of class where to search
        :param value: value to check for existence
        :param operator_: callable operator
        :return: result of query
        """
        return cls.select().where(operator_(cls.get_attrib(field), value))

    @classmethod
    def get_with_operator(cls, field, value, operator_: callable = operator.eq):
        """ Retrieves list of objects filtered by given field *operator* value

        :param field: field of class where to search
        :param value: value to check for existence
        :param operator_:
        :return: result of query
        """
        return [obj for obj in cls.select_with_operator(field, value, operator_)]

    @classmethod
    def exists(cls, field, value):
        """ Check if given field and value exists

        :param field: field of class where to search
        :param value: value to check for existence
        :return: True if exists else False
        """
        return cls.select_with_operator(field, value, operator.eq).exists()

    @classmethod
    def select_equals(cls, field, value):
        """ Select query and filter by given field == value

        :param field: field of class where to search
        :param value: value to check for existence
        :return: ModelSelect object with result of query
        """
        return cls.select_with_operator(field, value, operator.eq)

    @classmethod
    def get_single(cls, field, value):
        """ Getting single result of query matching field == value

        :param field: field of class where to search
        :param value: value to check for existence
        :return: Correspondent queried object or empty list
        """
        return cls.select_with_operator(field, value, operator.eq).get()

    @classmethod
    def get_equals(cls, field, value):
        """ Getting result of query matching field == value

        :param field: field of class where to search
        :param value: value to check for existence
        :return: Correspondent queried object or empty list
        """
        return [obj for obj in cls.select_equals(field, value)]

    @classmethod
    def get_attrib(cls, field):
        """ Getting attribute of class
        Helper function for select_with_operator() method

        :param field: field of class
        :return: correspondent field of class
        :raises: exception if class doesn`t have given field
        """
        return getattr(cls, field.__dict__['name'])
