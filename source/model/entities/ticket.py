from datetime import datetime
from typing import List
from uuid import uuid4, UUID

from peewee import TextField, ForeignKeyField, DateTimeField, CharField

from source.config import Config
from source.model.entities import DatabaseModel, with_connection, ServiceUser


class TicketStatus(object):
    """ Helper class to deal with ticket statuses
    """
    postponed = Config().assets.ticket_postponed
    pending = Config().assets.ticket_pending
    done = Config().assets.ticket_done
    declined = Config().assets.ticket_declined

    opened = (pending, postponed)
    closed = (declined, done)

    operations = (postponed, done, declined)
    statuses = [postponed, pending, done, declined]

    mapping = {i: status for i, status in enumerate(statuses)}

    @staticmethod
    def get_and_(status):
        """ Helper for call appropriate method, based on status to change
        :param status: Status to apply to ticket
        :return: callable of chosen status
        """
        status = status[1:] if not status.isalpha() else status
        mapping = {
            TicketStatus.postponed: Ticket.get_and_postpone,
            TicketStatus.done: Ticket.get_and_done,
            TicketStatus.declined: Ticket.get_and_decline,
        }
        return mapping.get(status, None)


class Ticket(DatabaseModel):
    """ Represents table ticket
    """
    uuid = CharField(primary_key=True)
    uuid_short = CharField(index=True, unique=True)
    description = TextField()
    created_by = ForeignKeyField(ServiceUser, ServiceUser.id, backref='tickets', index=True)
    created_at = DateTimeField(index=True)
    status = CharField(index=True)
    last_activity = DateTimeField()

    _uid_len = 36
    _uid_short_len = 12

    @property
    def creator(self):
        return self.created_by

    @property
    def creator_name(self):
        user = self.created_by
        return f'{user.first_name} {user.last_name}'

    @property
    def status_log(self):
        """ Retreives last ticket status and its description
        :return: TicketStatusLog object
        """
        if self.status in TicketStatus.operations:
            from source.model.entities import TicketStatusLog
            return (
                TicketStatusLog
                .select()
                .where(
                    (TicketStatusLog.ticket_id == self.uuid_short)
                )
                .order_by(TicketStatusLog.date.desc())
                .get()
            )
        return None

    @property
    def postponed_by_date(self):
        if self.status == TicketStatus.postponed:
            from source.model.entities import TicketStatusLog
            return (
                TicketStatusLog
                .select()
                .where(
                    (TicketStatusLog.ticket_id == self.uuid_short)
                    & (TicketStatusLog.status == TicketStatus.postponed)
                )
                .order_by(TicketStatusLog.date.desc())
                .get().postponed_by_date
            )
        return None

    def postpone(self, description: str, by_date: datetime):
        """ Postpone ticket and write new record to log
        :param description: given description to status change
        :param by_date: by what date to postpone
        :return: rows affected
        """
        from source.model.entities import TicketStatusLog
        self.status = TicketStatus.postponed
        self.last_activity = datetime.now()
        TicketStatusLog.new(self, description, by_date)
        return self.save()

    def done(self, description: str):
        """ Change ticket status to done, write new record to log
        :param description: given description to status change
        :return: rows affected
        """
        from source.model.entities import TicketStatusLog
        self.status = TicketStatus.done
        self.last_activity = datetime.now()
        TicketStatusLog.new(self, description)
        return self.save()

    def decline(self, description: str):
        """ Change ticket status to declined, write new record to log
        :param description: given description to status change
        :return: rows affected
        """
        from source.model.entities import TicketStatusLog
        self.status = TicketStatus.declined
        self.last_activity = datetime.now()
        TicketStatusLog.new(self, description)
        return self.save()

    @classmethod
    def _get_by_uuid(cls, uid: str) -> 'Ticket':
        """ Getting ticket by uuid
        :param uid: given uuid
        :return: ticket object
        """
        if len(uid) == Ticket._uid_len:
            return Ticket.get_single(Ticket.uuid, uid)
        elif len(uid) == Ticket._uid_short_len:
            return Ticket.get_single(Ticket.uuid_short, uid)

    @classmethod
    @with_connection()
    def get_by_uuid(cls, uid: str) -> 'Ticket':
        """ shortcut for getting ticket by its uuid
        :param uid: ticket uuid
        :return: ticket object
        """
        return cls._get_by_uuid(uid)

    @classmethod
    @with_connection()
    def get_all_by_userid(cls, user_id: int) -> List['Ticket']:
        """ Getting all tickets created by user_id
        :param user_id: given user id
        :return: list of tickets created by user
        """
        return [ticket for ticket in Ticket.select_equals(Ticket.created_by, user_id)]

    @classmethod
    @with_connection()
    def get_all_opened_by_userid(cls, user_id: int) -> List['Ticket']:
        """ Getting all opened ticket by user_id
        :param user_id: given user id
        :return: list of open ticket
        """
        query = (
            Ticket
                .select()
                .where(Ticket.created_by == user_id, Ticket.status.in_(TicketStatus.opened))
        )
        return [ticket for ticket in query]

    @classmethod
    @with_connection()
    def get_all_opened(cls) -> List['Ticket']:
        """ Getting all opened tickets
        :return: list of open tickets
        """
        query = (
            Ticket
                .select()
                .where(Ticket.status.in_(TicketStatus.opened))
                .order_by(Ticket.status.asc())
        )
        return [ticket for ticket in query]

    @classmethod
    @with_connection()
    def get_and_postpone(cls, uid: str, description: str, by_date: datetime) -> 'Ticket':
        """ Get and change ticket status
        :param uid: ticket uuid
        :param description: description to status change
        :param by_date: postpone by date
        :return: affected ticket object
        """
        obj = cls._get_by_uuid(uid)
        if obj:
            obj.last_activity = datetime.now()
            obj.postpone(description, by_date)
            return obj

    @classmethod
    @with_connection()
    def get_and_decline(cls, uid: str, description: str) -> 'Ticket':
        """ Get and change ticket status
        :param uid: ticket uuid
        :param description: description to status change
        :return: affected ticket object
        """
        obj = cls._get_by_uuid(uid)
        if obj:
            obj.last_activity = datetime.now()
            obj.decline(description)
            return obj

    @classmethod
    @with_connection()
    def get_and_done(cls, uid: str, description: str) -> 'Ticket':
        """ Get and change ticket status
        :param uid: ticket uuid
        :param description: description to status change
        :return: affected ticket object
        """
        obj = cls._get_by_uuid(uid)
        if obj:
            obj.last_activity = datetime.now()
            obj.done(description)
            return obj

    @staticmethod
    @with_connection()
    def new(description: str, user_id: int, date: datetime = None):
        """ Create new ticket
        :param description: ticket description
        :param user_id: user id ticket was created by
        :param date: date of creation
        :return: Created ticket object
        """
        if ServiceUser.exists(ServiceUser.id, user_id):
            from source.model.entities import TicketStatusLog
            obj = TicketBuilder() \
                    .description(description) \
                    .created_at(date) \
                    .created_by(user_id) \
                    .build()
            TicketStatusLog.new(obj, None)
            return obj

    def __repr__(self):
        return f'Ticket(uuid_short: {self.uuid_short}, ' \
            f'description: {self.description}, ' \
            f'created_by {self.created_by}, ' \
            f'created_at {self.created_at}, ' \
            f'status {self.status}, ' \
            f'last_activity {self.last_activity}, ' \
            f'uuid: {self.uuid})'

    def __str__(self):
        return (
            f'uuid_short: {self.uuid_short}, '
            f'description: {self.description}, '
            f'created_by {self.created_by}, '
            f'created_at {self.created_at}, '
            f'status {self.status}, '
            f'last_activity {self.last_activity})'
        )

    def to_html(self, user: str):
        """ Wraps Ticket object fields with html tags
        :param user: user first and last names
        :return: string with html wrapped fields
        """
        on_status = self.status_log_string()
        return (
                f'<b>статус:</b> {self.status}' + '\n'
                f'{on_status}'
                f'<b>опис:</b> {self.description}' + '\n'
                f'<b>працівник:</b> {user}' + '\n'
                f'<b>створено:</b> {self.created_at.strftime(Config().pretty_datetime)}' + '\n'
                f'<b>ID:</b> {self.uuid_short}' + '\n'
        )

    def status_log_string(self):
        """ Converts status and its description to string
        :return: String with self status_description and postponed_by_date if object is postponed
        """
        if self.status_log:
            if self.status_log.status == TicketStatus.postponed:
                date_str = self.status_log.postponed_by_date.strftime(Config().pretty_date)
                return f'<b>коментар:</b> {self.status_log.status_description} <b>відкладено на {date_str}</b>' + '\n'
            else:
                return f'<b>коментар:</b> {self.status_log.status_description}' + '\n'
        else:
            return ''


class TicketBuilder(object):
    """ Helper class for ticket creation
    """
    def __init__(self):
        self.__uid = None
        self.__uid_short = None
        self.__description = None
        self.__created_by = None
        self.__created_at = None
        self.__status = None
        self.__is_open = None
        self.__last_activity = None

    def uid(self, uid=None):
        """ Create ticket uuid object
        :param uid: uuid to init ticket object with, if None - generated automatically
        """
        if not uid:
            self.__uid = str(uuid4())
            self.__uid_short = self.__uid[-Ticket._uid_short_len:]
        else:
            if isinstance(uid, UUID):
                self.__uid = str(uid)
                self.__uid_short = self.__uid[-Ticket._uid_short_len:]
            elif isinstance(uid, str) and len(uid) == Ticket._uid_len:
                self.__uid = uid
                self.__uid_short = self.__uid[-Ticket._uid_short_len:]
            else:
                raise TypeError('must be 32 chars string or UUID object')

    def description(self, description: str):
        """ Ticket description
        :param description: user description of problem
        :return: builder object
        """
        self.__description = description
        return self

    def created_by(self, created_by: int):
        """ Ticket created by init
        :param created_by: user id
        :return: builder object
        """
        self.__created_by = created_by
        return self

    def created_at(self, created_at: datetime):
        """ Ticket created_at field init
        :param created_at: datetime of ticket creation
        :return: builder object
        """
        if not created_at:
            created_at = datetime.now()
        self.__created_at = created_at
        self.__last_activity = created_at
        return self

    def build(self):
        """ Builds Ticket object
        :return: Created Ticket object
        """
        if not self.__uid:
            self.uid()
        return Ticket.create(uuid=self.__uid,
                             uuid_short=self.__uid_short,
                             description=self.__description,
                             created_by=self.__created_by,
                             created_at=self.__created_at,
                             status=TicketStatus.pending,
                             is_open=True,
                             last_activity=self.__last_activity)
