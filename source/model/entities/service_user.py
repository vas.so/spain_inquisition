from typing import List

from peewee import CharField, BigIntegerField, BooleanField
from telegram import User

from source.model.entities import DatabaseModel, with_connection
from source.utils import user_mention


class ServiceUser(DatabaseModel):
    """ Model class represents table of service users
    """
    id = BigIntegerField(unique=True, primary_key=True)
    first_name = CharField(null=True)
    last_name = CharField(null=True)
    is_active = BooleanField(index=True, default=True)
    is_manager = BooleanField(index=True, default=False)
    is_support = BooleanField(index=True, default=False)

    @property
    def full_name(self) -> str:
        """ Shortcut for full name
        :return: concat first_name + last_name
        """
        return f'{self.first_name} {self.last_name}'

    @property
    def has_real_name(self) -> bool:
        """ Check if user has name
        :return: True - if name is not None, else - False
        """
        return self.first_name and self.last_name is not None

    @property
    def self_ticket_list(self) -> list:
        """ user tickets
        :return: getting all tickets list created by this user with status 'opened'
        """
        from source.model.entities import Ticket, TicketStatus
        return [obj for obj in Ticket.select().where(
            (Ticket.created_by == self.telegram_id) &
            (Ticket.status.in_(TicketStatus.opened))
        )]

    def make_inactive(self):
        """  Change state to inactive, used when user is deleted from group
        """
        self.is_active = self.is_manager = self.is_support = False
        self.save()

    def make_active(self):
        """  Change state to active
        """
        self.is_active = True
        self.save()

    def update_name(self, first_name: str, last_name: str):
        """ Updates existed user with given first and last names
        :param first_name: First name given
        :param last_name:  Last name given
        :return: Rows affected, or None on error
        """
        self.first_name = first_name.lower().capitalize()
        self.last_name = last_name.lower().capitalize()
        self.save()

    def set_privilegies(self, is_manager: bool = False, is_support: bool = False, ):
        """ Updates existed user with given first and last names
        :param is_manager:
        :param is_support:
        :return: Rows affected, or None on error
        """
        self.is_manager = is_manager
        self.is_support = is_support
        self.save()

    def to_html(self) -> str:
        """ Wraps object with html tags
        :return: string with needed object properties wrapped in html
        """
        if_manager = f'manager' if self.is_manager else ''
        if_support = f'support' if self.is_support else ''
        mention = user_mention(self.id, f'{self.first_name} {self.last_name}')
        return f'<b>{self.id}</b> {mention} {if_manager} {if_support}'

    @classmethod
    def check_manager(cls, user_id: int) -> bool:
        """ Check if user with given id is manager
        :param user_id: User telegram id
        :return: True - is manager, else - False
        """
        user = cls.get(user_id)
        return user and user.is_manager

    @classmethod
    def check_support(cls, user_id: int) -> bool:
        """ Check if user with given id is support
        :param user_id: User telegram id
        :return: True - is support, else - False
        """
        user = cls.get(user_id)
        return user and user.is_support

    @classmethod
    def get_by_id(cls, user_id) -> 'ServiceUser' or None:
        """ Getting user with given id
        :param user_id: User telegram id
        :return: ServiceUser object if exists, else - None
        """
        if cls.exists(cls.id, user_id):
            return cls.select().where(cls.id == user_id).get()
        else:
            return None

    @classmethod
    @with_connection()
    def get_supports(cls) -> List['ServiceUser']:
        """ Getting list of all supports
        :return: List of supports objects
        """
        return [support for support in cls.select().where((cls.is_support == True) & (cls.is_active == True))]

    @classmethod
    @with_connection()
    def get_all(cls) -> List['ServiceUser']:
        """ Getting all users present
        :return: List of ServiceUser objects
        """
        return [user for user in cls.select().where(cls.is_active == True)]

    @classmethod
    def exists_with_id(cls, user_id) -> bool:
        """ Check if given id exists in ServiceUser table
        :param user_id: User id to check
        :return: True - if exists, else - False
        """
        return cls.exists(cls.id, user_id)

    @classmethod
    @with_connection()
    def exists_get(cls, user_id) -> 'ServiceUser' or None:
        """ Retreiving User object by its ID

        :param user_id: Given User ID
        :return: Arbitrary User object or None if not exist
        """
        if cls.exists_with_id(user_id):
            return cls.get_by_id(user_id)
        return None

    @staticmethod
    @with_connection()
    def add(user: User) -> 'ServiceUser':
        """ Add new user to database
        :param user: telegram.User object
        :return: Created ServiceUser object
        """
        if not ServiceUser.exists_with_id(user.id):
            return ServiceUser.create(id=user.id)

    def __repr__(self):
        return (
            f'ServiceUser(id: {self.id}, '
            f'first_name: {self.first_name}, '
            f'last_name: {self.last_name}) '
            f'is_active: {self.is_active}) '
            f'is_support: {self.is_support}) '
            f'is_manager: {self.is_manager}) '
        )

    def __str__(self):
        return (
            f'ServiceUser(id: {self.id}, '
            f'first_name: {self.first_name}, '
            f'last_name: {self.last_name})'
        )
