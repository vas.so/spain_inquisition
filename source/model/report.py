from datetime import datetime
from typing import List

import xlsxwriter
from babel.dates import format_datetime

from source.config import Config
from source.model.datetime_utils import Datetime
from source.model.entities import ServiceUser, TicketStatusLog
from source.model.entities.database import get_pooled_database
from source.utils import log


def to_xlsx(ticket_logs: List[TicketStatusLog]) -> xlsxwriter.Workbook or None:
    """ Writes collection of ticket logs to xlsx file
    :param ticket_logs: Collection of ticket logs to write
    :return: xlsx file created
    """
    if len(ticket_logs) > 0:
        column_names = Config().assets.ticket_status_columns

        workbook = xlsxwriter.Workbook(datetime.now().strftime(Config().xlsx_date))
        worksheet = workbook.add_worksheet()

        cell_base_format = workbook.add_format()
        cell_base_format.set_align('center')
        cell_base_format.set_align('vcenter')

        cell_date_format = workbook.add_format()
        cell_date_format.set_num_format('hh:mm dd/mmmm/yyyy AM/PM')
        cell_date_format.set_align('centre')

        worksheet.set_row(0, 70)
        worksheet.set_column(0, len(column_names), 30)

        [worksheet.write(0, index, column_names[key], cell_base_format) for index, key in enumerate(column_names)]
        for row, ticket in enumerate(ticket_logs):
            row = row + 1
            for column, key in enumerate(column_names):
                field = getattr(ticket, key)
                if type(field) == ServiceUser:
                    worksheet.write(row, column, field.full_name)
                elif type(field) == datetime:
                    date_formatted = format_datetime(field, locale=Config().main_locale)
                    worksheet.write(row, column, date_formatted, cell_date_format)
                elif key == 'status':
                    if ticket.postponed_by_date:
                        postponed_by = 'на ' + ticket.postponed_by_date.strftime(Config().pretty_date)
                    else:
                        postponed_by = ''
                    worksheet.write(row, column, f'{field} {postponed_by}')
                else:
                    worksheet.write(row, column, field)
        workbook.close()
        return workbook
    else:
        return None


class Report(object):
    """ Helper class for making reports
    """

    @classmethod
    @log
    def day_map(cls, make_for: str):
        """ Mapping report method for given date range
        :param make_for: date range
        :return: appropriate callable or this_day as default
        """
        return {
            Config().assets.this_day: cls.this_day,
            Config().assets.this_week: cls.this_week,
            Config().assets.yesterday: cls.yesterday,
            Config().assets.this_month: cls.this_month,
            Config().assets.custom: cls.custom,
        }.get(make_for, cls.custom)

    @classmethod
    @log
    def this_day(cls, *args, **kwargs) -> List[TicketStatusLog]:
        """ Shortcut for Report on current day
        :return: Collection of tickets changed this day
        """
        today = Datetime.today()
        return cls.custom(today, Datetime.now())

    @classmethod
    @log
    def yesterday(cls, *args, **kwargs) -> List[TicketStatusLog]:
        """ Shortcut for Report on yesterday
        :return: Collection of tickets changed yesterday
        """
        yesterday, today = Datetime.yesterday(), Datetime.now()
        return cls.custom(yesterday, today)

    @classmethod
    @log
    def this_week(cls, *args, **kwargs) -> List[TicketStatusLog]:
        """ Shortcut for Report on current week
        :return: Collection of tickets changed this week
        """
        week_start, today = Datetime.week(), Datetime.now()
        return cls.custom(week_start, today)

    @classmethod
    @log
    def this_month(cls, *args, **kwargs) -> List[TicketStatusLog]:
        """ Shortcut for Report on current month
        :return: Collection of tickets changed current month
        """
        month_start, today = Datetime.month(), Datetime.now()
        return cls.custom(month_start, today)

    @classmethod
    @log
    def custom(cls, from_date: datetime, to_date: datetime) -> List[TicketStatusLog]:
        """ Report for custom period
        :param from_date: start date
        :param to_date: finish date
        :return: Collection of tickets changed on from_date to to_date
        """
        with get_pooled_database() as db:
            with db.atomic():
                query = (
                    TicketStatusLog
                    .select()
                    .where(TicketStatusLog.date.between(from_date, to_date))
                )
        return [ticket for ticket in query]
