from datetime import datetime, time, date, timedelta
from typing import Tuple

from dateutil.relativedelta import relativedelta


def date_convert(date_):
    dt = date_.split('/')
    if len(dt) == 1:
        today = Datetime().today()
        return DayMonthYear(int(dt[0]), today.month, today.year).date()
    elif len(dt) == 2:
        today = Datetime().today()
        return DayMonthYear(int(dt[0]), int(dt[1]), today.year).date()
    elif len(dt) == 3:
        return DayMonthYear(int(dt[0]), int(dt[1]), int(dt[2])).date()
    else:
        raise ValueError


class DayMonthYear():
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year

    def date(self):
        return date(day=self.day, month=self.month, year=self.year)

    def to_dict(self):
        return {
            'day': self.day,
            'month': self.month,
            'year': self.year,
        }


class Datetime(object):
    """ Helper class for high level date manipulation

    """

    days = ['Monday', 'Tuesday', 'Wednesday', 'Thurdsay', 'Friday', 'Saturday', 'Sunday']
    Monday, Tuesday, Wednesday, Thurdsay, Friday, Saturday, Sunday = range(1, 8)
    day_of_week = {index + 1: value for index, value in enumerate(days)}

    @staticmethod
    def from_days_or_time(days: int = None, hours: int = None):
        if days or days == 0:
            if hours:
                return datetime.now() + timedelta(days=days, hours=hours)
            else:
                return datetime.now() + timedelta(days=days)
        elif hours:
            return datetime.now() + timedelta(hours=hours)

    @staticmethod
    def today_from_hour(hour: int = 0) -> datetime:
        now_ = datetime.now()
        hour_ = time(hour, now_.minute, now_.second)
        return datetime.combine(now_, hour_)

    @staticmethod
    def datetime_before_today(days: int = 0,
                              from_hour: int = 0) -> datetime:
        day_before = relativedelta(days=days)
        return Datetime.today_from_hour(from_hour) - day_before

    @staticmethod
    def now():
        return datetime.now()

    @staticmethod
    def is_working_hours():
        dt = datetime.now()
        return dt.hour in range(9, 19)

    @staticmethod
    def today(from_hour: int = 0) -> datetime:
        return Datetime.today_from_hour(hour=from_hour)

    @staticmethod
    def yesterday(from_hour: int = 0) -> datetime:
        return Datetime.datetime_before_today(days=1, from_hour=from_hour)

    @staticmethod
    def tomorrow(from_hour: int = 0) -> datetime:
        return Datetime.today() + relativedelta(days=1)

    @staticmethod
    def week() -> datetime:
        return Datetime.datetime_before_today(days=date.today().weekday())

    @staticmethod
    def month() -> datetime:
        return Datetime.datetime_before_today(days=date.today().day - 1)

    @staticmethod
    def custom(from_day: date or DayMonthYear,
               to_day: date or DayMonthYear) -> Tuple[datetime, datetime]:
        return Datetime.__patch_custom_date(from_day, to_day)

    @staticmethod
    def custom_to_today(to_day: date or DayMonthYear):
        Datetime.custom(date.today(), to_day)

    @staticmethod
    def __patch_custom_date(from_day, to_day) -> Tuple[datetime, datetime]:
        if all([isinstance(from_day, date), isinstance(to_day, date)]):
            return (
                datetime.combine(from_day, time(0)),
                datetime.combine(to_day, time(0))
            )
        elif all([isinstance(from_day, DayMonthYear), isinstance(to_day, DayMonthYear)]):
            return (
                datetime(**from_day.to_dict()),
                datetime(**to_day.to_dict())
            )
